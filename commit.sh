set -e
git diff --staged > changes.diff
wd=$(pwd)
if [ -d $wd-tmp ]; then
    rm -rf $wd-tmp
fi
mkdir $wd-tmp
git clone $wd $wd-tmp
cd $wd-tmp
git apply $wd/changes.diff
mvn clean install
mvn spring-boot:run &
SERVER_PID=$!
curl --retry 4 --retry-connrefused localhost:8080/health-check
kill $SERVER_PID
cd $wd
rm -rf $wd-tmp
git commit -m "$*"


