package com.clau495h.filediffer;

import java.util.UUID;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FileControllerIntegrationTest {

    private static final String LOCALHOST = "http://localhost:";
    private static final String TEST = "/v1/diff/test";
    @LocalServerPort
    private int port;
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void testUploadFile() {
        FileBody fileBody = new FileBody(UUID.randomUUID().toString().getBytes());
        String url = LOCALHOST + port + TEST + "/left";
        ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(fileBody), String.class);
        Assertions.assertEquals("OK", exchange.getBody());
    }

    @Test
    public void testDiffFileEquals() {
        FileBody fileBody = new FileBody(UUID.randomUUID().toString().getBytes());
        String leftUrl = LOCALHOST + port + TEST + "/left";
        String rightUrl = LOCALHOST + port + TEST + "/right";
        String diffUrl = LOCALHOST + port + TEST;
        ResponseEntity<String> exchange = restTemplate.exchange(rightUrl, HttpMethod.PUT, new HttpEntity<>(fileBody), String.class);
        Assertions.assertEquals("OK", exchange.getBody());
        exchange = restTemplate.exchange(leftUrl, HttpMethod.PUT, new HttpEntity<>(fileBody), String.class);
        Assertions.assertEquals("OK", exchange.getBody());
        ResponseEntity<DiffResponse> diffResponse = restTemplate.exchange(diffUrl, HttpMethod.GET, new HttpEntity<>(fileBody), DiffResponse.class);
        Assertions.assertTrue(diffResponse.getBody().isEqualSize());
        Assertions.assertTrue(diffResponse.getBody().isEquals());
        Assertions.assertTrue(diffResponse.getBody().getDifferences().isEmpty());
    }

    @Test
    public void testDiffFileSameSizeDifferent() {
        FileBody leftBody = new FileBody(UUID.randomUUID().toString().getBytes());
        FileBody rightBody = new FileBody(UUID.randomUUID().toString().getBytes());
        String leftUrl = LOCALHOST + port + TEST + "/left";
        String rightUrl = LOCALHOST + port + TEST + "/right";
        String diffUrl = LOCALHOST + port + TEST;
        ResponseEntity<String> exchange = restTemplate.exchange(rightUrl, HttpMethod.PUT, new HttpEntity<>(rightBody), String.class);
        Assertions.assertEquals("OK", exchange.getBody());
        exchange = restTemplate.exchange(leftUrl, HttpMethod.PUT, new HttpEntity<>(leftBody), String.class);
        Assertions.assertEquals("OK", exchange.getBody());
        ResponseEntity<DiffResponse> diffResponse = restTemplate.exchange(diffUrl, HttpMethod.GET, new HttpEntity<>(leftBody), DiffResponse.class);
        Assertions.assertTrue(diffResponse.getBody().isEqualSize());
        Assertions.assertFalse(diffResponse.getBody().isEquals());
        Assertions.assertFalse(diffResponse.getBody().getDifferences().isEmpty());
    }

    @Test
    public void testDiffFileDifferentSize() {
        FileBody leftBody = new FileBody(UUID.randomUUID().toString().substring(1).getBytes());
        FileBody rightBody = new FileBody(UUID.randomUUID().toString().getBytes());
        String leftUrl = LOCALHOST + port + TEST + "/left";
        String rightUrl = LOCALHOST + port + TEST + "/right";
        String diffUrl = LOCALHOST + port + TEST;
        ResponseEntity<String> exchange = restTemplate.exchange(rightUrl, HttpMethod.PUT, new HttpEntity<>(rightBody), String.class);
        Assertions.assertEquals("OK", exchange.getBody());
        exchange = restTemplate.exchange(leftUrl, HttpMethod.PUT, new HttpEntity<>(leftBody), String.class);
        Assertions.assertEquals("OK", exchange.getBody());
        ResponseEntity<DiffResponse> diffResponse = restTemplate.exchange(diffUrl, HttpMethod.GET, new HttpEntity<>(leftBody), DiffResponse.class);
        Assertions.assertFalse(diffResponse.getBody().isEqualSize());
        Assertions.assertFalse(diffResponse.getBody().isEquals());
        Assertions.assertTrue(diffResponse.getBody().getDifferences().isEmpty());
    }
}
