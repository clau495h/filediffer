package com.clau495h.filediffer;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class FileControllerTest {

    @Test
    public void testHealthCheck() {
        System.out.println("healthCheck");
        FileController instance = new FileController();
        String expResult = "OK";
        String result = instance.healthCheck();
        Assertions.assertEquals(expResult, result);
    }

    @Test
    public void testDiffCompareEquals() {
        System.out.println("diff compare different");
        FileController instance = new FileController();
        String test = "test";
        instance.uploadSide(test, "left", new FileBody("atado".getBytes()));
        instance.uploadSide(test, "right", new FileBody("atado".getBytes()));
        DiffResponse result = instance.diff(test);
        DiffResponse expected = new DiffResponse(true, true, Collections.emptySet());
        Assertions.assertArrayEquals(expected.getDifferences().toArray(), result.getDifferences().toArray(), "wrong result");
        Assertions.assertEquals(expected.isEquals(), result.isEquals());
        Assertions.assertEquals(expected.isEqualSize(), result.isEqualSize());
    }

    @Test
    public void testDiffCompareEqualSizeDifferent() {
        System.out.println("diff compare equal size different");
        FileController instance = new FileController();
        String test = "test";
        String leftSide = UUID.randomUUID().toString();
        String rightSide = UUID.randomUUID().toString();
        instance.uploadSide(test, "left", new FileBody(leftSide.getBytes()));
        instance.uploadSide(test, "right", new FileBody(rightSide.getBytes()));
        DiffResponse result = instance.diff(test);
        Assertions.assertFalse(result.isEquals());
        Assertions.assertTrue(result.isEqualSize());
        result.getDifferences().forEach(d -> {
            String leftSubstring = leftSide.substring(d.getOffset(), d.getOffset() + d.getLength());
            String rightSubstring = rightSide.substring(d.getOffset(), d.getOffset() + d.getLength());
            Assertions.assertNotEquals(leftSubstring, rightSubstring);
        });
        instance.uploadSide(test, "right", new FileBody(leftSide.getBytes()));
        instance.uploadSide(test, "left", new FileBody(rightSide.getBytes()));
        DiffResponse inverted = instance.diff(test);
        Assertions.assertFalse(inverted.isEquals());
        Assertions.assertTrue(inverted.isEqualSize());
        inverted.getDifferences().forEach(d -> {
            String leftSubstring = leftSide.substring(d.getOffset(), d.getOffset() + d.getLength());
            String rightSubstring = rightSide.substring(d.getOffset(), d.getOffset() + d.getLength());
            Assertions.assertNotEquals(leftSubstring, rightSubstring);
        });
        Assertions.assertArrayEquals(result.getDifferences().toArray(), inverted.getDifferences().toArray());
    }

    public void testDiffCompareDifferentSize() {
        System.out.println("diff compare different size");
        FileController instance = new FileController();
        String test = "test";
        instance.uploadSide(test, "left", new FileBody("abacos".getBytes()));
        instance.uploadSide(test, "right", new FileBody("atado".getBytes()));
        DiffResponse result = instance.diff(test);
        DiffResponse expected = new DiffResponse(false, false, Collections.emptySet());
        Assertions.assertArrayEquals(expected.getDifferences().toArray(), result.getDifferences().toArray(), "wrong result");
        Assertions.assertEquals(expected.isEquals(), result.isEquals());
        Assertions.assertEquals(expected.isEqualSize(), result.isEqualSize());
    }

    @Test
    public void testCompare() {
        System.out.println("compare");
        byte[] left = "atado".getBytes();
        byte[] right = "abaco".getBytes();
        FileController instance = new FileController();
        Set<Difference> expResult = buildExpected();
        Set<Difference> result = instance.compare(left, right);
        Assertions.assertArrayEquals(expResult.toArray(), result.toArray());
    }

    private Set<Difference> buildExpected() {
        Set<Difference> expResult = new LinkedHashSet<>(16);
        expResult.add(new Difference(1, 1));
        expResult.add(new Difference(3, 1));
        return expResult;
    }
}
