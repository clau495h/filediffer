package com.clau495h.filediffer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileDifferApp {

    public static void main(String[] args) {
        SpringApplication.run(FileDifferApp.class, args);
    }
}
