package com.clau495h.filediffer;

import io.swagger.annotations.ApiOperation;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FileController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileController.class);
    private static final String UPLOADS_FOLDER = "uploads";

    public FileController() {
        new File(UPLOADS_FOLDER).mkdir();
    }

    @GetMapping("/health-check")
    @ApiOperation(value = "health-check for monitoring liveness")
    public String healthCheck() {
        return "OK";
    }

    @PutMapping(
            path = "/v1/diff/{id}/{side:(?:left|right)}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.TEXT_PLAIN_VALUE)
    @ApiOperation(value = "upload either right or left side for comparison")
    public String uploadSide(@PathVariable String id, @PathVariable String side, @RequestBody FileBody fileBody) {
        LOGGER.debug("id: {}, size: {}", id, fileBody.getBody().length);
        try {
            final Path filePath = FileSystems.getDefault().getPath(UPLOADS_FOLDER, id + side);
            Files.deleteIfExists(filePath);
            Files.createFile(filePath);
            Files.write(filePath, fileBody.getBody());
            return "OK";
        } catch (IOException ex) {
            LOGGER.error("Error saving file", ex);
            throw new RuntimeException(ex);
        }
    }

    @GetMapping(path = "/v1/diff/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "compare right and left")
    public DiffResponse diff(@PathVariable String id) {
        try {
            LOGGER.debug("id: {}", id);
            final Path leftPath = FileSystems.getDefault().getPath(UPLOADS_FOLDER, id + "left");
            long leftSize = Files.size(leftPath);
            final Path rightPath = FileSystems.getDefault().getPath(UPLOADS_FOLDER, id + "right");
            long rightSize = Files.size(rightPath);
            if (leftSize == rightSize) {
                final byte[] leftBytes = Files.readAllBytes(leftPath);
                final byte[] rightBytes = Files.readAllBytes(rightPath);
                Set<Difference> differences = compare(leftBytes, rightBytes);
                return new DiffResponse(differences.isEmpty(), true, differences);
            } else {
                return new DiffResponse(false, false);
            }
        } catch (IOException ex) {
            LOGGER.error("error diffing files", ex);
            throw new RuntimeException(ex);
        }
    }

    Set<Difference> compare(byte[] left, byte[] right) {
        Set<Difference> differences = new LinkedHashSet<>(16);
        int currLength = 0;
        for (int i = 0; i < left.length; i++) {
            if (left[i] == right[i] || (i == left.length - 1)) {
                if (currLength > 0) {
                    differences.add(new Difference(i - currLength, currLength));
                    currLength = 0;
                }
            } else {
                ++currLength;
            }
        }
        return differences;
    }
}
