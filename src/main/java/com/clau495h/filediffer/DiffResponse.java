package com.clau495h.filediffer;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DiffResponse {

    private boolean equals;
    private boolean equalSize;
    private Set<Difference> differences = new HashSet<>(16);

    //for testing
    DiffResponse() {
    }

    DiffResponse(boolean equals, boolean equalSize) {
        this.equals = equals;
        this.equalSize = equalSize;
    }

    DiffResponse(boolean equals, boolean equalSize, Set<Difference> differences) {
        this.equals = equals;
        this.equalSize = equalSize;
        this.differences = differences;
    }

    public boolean isEquals() {
        return equals;
    }

    public boolean isEqualSize() {
        return equalSize;
    }

    public Set<Difference> getDifferences() {
        return Collections.unmodifiableSet(differences);
    }
}
