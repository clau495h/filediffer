package com.clau495h.filediffer;

public class FileBody {

    private byte[] body;

    public FileBody() {
        //for serialization
    }

    //for testing
    FileBody(byte[] body) {
        this.body = body;
    }

    public byte[] getBody() {
        return body;
    }
}
