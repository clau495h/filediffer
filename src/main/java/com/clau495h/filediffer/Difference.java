package com.clau495h.filediffer;

public class Difference {

    private int offset;
    private int length;

    //for testing
    public Difference() {
    }

    Difference(int offset, int length) {
        this.offset = offset;
        this.length = length;
    }

    public int getOffset() {
        return offset;
    }

    public int getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "offset: " + offset + ", length: " + length;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.offset;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Difference other = (Difference) obj;
        if (this.offset != other.offset) {
            return false;
        }
        return true;
    }
}
