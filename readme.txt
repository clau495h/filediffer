file differ

instructions to run the project

1- clone the project
2- mvn clean install
3- java -jar target/fileDiffer-0.1-SNAPSHOT-spring-boot.jar
4- check http://localhost:8080/swagger-ui.html for documentation on usage and to test requests


Sugestions on improvements

1- chunked handling of diffing and uploading to avoid memory issues with big files
2- use MemoryMapped files to improve performance on the diff
3- improved multithreaded algorithm instead of single threaded as it is now, for example dividing the file (or file chunk) in regions, 1 region per available processor,
	this would require to merge each region last value with next region first value, which seems cheap for the added performance of parallelism.
4- could use and array to save offset + length in pairs of adjacent indexes to improve the pattern of memory usage,
	like this: [offset1, length1, offset2, length2, offset3, length3]
	this would require a custom serializer for the response but should improve performance nicely by making efficient use of processor caches
5- coud use a cassandra db to store the files so as to be able to scale horizontally both diff servers and storages servers
